from setuptools import setup

setup(
    name='silf',
    version='0.3.2',
    description='SILF application for django website',
    url='https://bitbucket.org/silf/silf-app',
    license='LICENSE.txt',
    packages=[
        'silf',
        'silf.core',
        'silf.reservation',
        'silf.tigase'
    ],
    install_requires=[
        'South',
        'djangorestframework>2.3.11,<2.4',
        'django-durationfield',
        'django-filter',
        'django-compressor',
        'mock',
        'numpy',
        'xlwt'
    ],
    package_data={
      'silf.reservation': ['static/silf/reservation/scripts/reservation.coffee']
    },
    zip_safe=False,
    include_package_data=True
)
