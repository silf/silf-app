"""
This settings module is used on semaphore CI to test silf-app when connected to postgresql database.
"""

from .settings_shared import *

import os

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

def get_env_variable(var_name, default=None):
    try:
        return os.environ[var_name]
    except KeyError:
        if default is None:
            error_msg = msg % var_name
            raise ImproperlyConfigured(error_msg)
        else:
            return default


# Make this unique, and don't share it with anybody.
SECRET_KEY = 'jcly1y@jmai416dgkqc#*)40krse=x67ds)((c52lnzv8vnlek'

DEBUG = True
TEMPLATE_DEBUG = DEBUG

TIGASE_SETUP_DB = False

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': get_env_variable('PGDATABASE'),
        'USER': get_env_variable('PGUSER'),
        'PASSWORD': get_env_variable('PGPASSWORD'),
        'HOST': get_env_variable('PGHOST'),
        'PORT': get_env_variable('PGPORT')
    }
}

TIGASE_SERVER_DOMAIN = 'localhost'
TIGASE_CONNECTION_URL = get_env_variable('TIGASE_CONNECTION_URL')

TIGASE_SETUP_DB = get_env_variable("TIGASE_SETUP_DB", "true").lower() == "true"
