from .settings_shared import *

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'jcly1y@jmai416dgkqc#*)40krse=x67ds)((c52lnzv8vnlek'

DEBUG = True
TEMPLATE_DEBUG = DEBUG

TIGASE_SETUP_DB = False

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',  # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'example.db',                      # Or path to database file if using sqlite3.
    }
}

TIGASE_SERVER_DOMAIN = "localhost"
TIGASE_CONNECTION_URL = "ws://127.0.0.1:5290"
