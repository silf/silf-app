"""
This settings module is used on semaphore CI to test silf-app when connected to sqlite database.
"""
from .settings_shared import *

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'jcly1y@jmai416dgkqc#*)40krse=x67ds)((c52lnzv8vnlek'

DEBUG = True
TEMPLATE_DEBUG = DEBUG

TIGASE_SETUP_DB = False

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',  # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'example.db',                      # Or path to database file if using sqlite3.
    }
}

# For some reason migrations are broken on SQLITE when you do them in one batch
# normally you can migrate them using two commands, but in tests you cant.
SOUTH_TESTS_MIGRATE = False

TIGASE_SERVER_DOMAIN = "localhost"
