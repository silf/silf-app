from django.conf.urls import include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = [
    url(r"^silf", include("silf.urls")),
    url(r'^admin/', include(admin.site.urls)),
    url(r"^auth/", include("django.contrib.auth.urls"))
]
