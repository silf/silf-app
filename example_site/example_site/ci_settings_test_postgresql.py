"""
This settings module is used on semaphore CI to test silf-app when connected to postgresql database.
"""

from .settings_shared import *

import os

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'jcly1y@jmai416dgkqc#*)40krse=x67ds)((c52lnzv8vnlek'

DEBUG = True
TEMPLATE_DEBUG = DEBUG

TIGASE_SETUP_DB = False

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'silfapp',
        'USER': os.environ['DATABASE_POSTGRESQL_USERNAME'],
        'PASSWORD': os.environ['DATABASE_POSTGRESQL_PASSWORD'],
    }
}

TIGASE_SERVER_DOMAIN = "localhost"
