Integration with tigase
=======================

This application has (optional) `Tigase XMPP Server
<www.tigase.org>`_ integration including:

* Generating Tigase users from django users.
* Keeps foreingn keys between these ysers.
* Managing passwords (separate from django passwords) of XMPP users

Requirements
------------

* This application uses postgres database,
* This application is installed in the same database and schema as tigase db.
* Django database user has right to execute: ``tigadduserplainpw``,
  and insert (update, delete) to all tables needed to create user. This
  should be ``tig_user`` and ``tig_node``.

How this works
--------------

This integration is turned on with ``TIGASE_SETUP_DB`` setting option.

This works as follows if ``TIGASE_SETUP_DB`` is false we create mock
``tig_users`` table, and then create entities inside.

If ``TIGASE_SETUP_DB`` is true **we assume that ``tig_users``** was created
by tigase setup, and only add do this table foreing key to ``django_user``.

You can switch from one option to the other (loosing all tigase users in the process)
by:

* migrating ``silf.tigase`` to ``zero``
* changing  ``TIGASE_SETUP_DB``
* migrating ``silf.tigase``

Database setup
--------------

You can either run migration for ``tigase`` app as an
administrative user or perform following script:

.. code-block:: sql

    ALTER table tig_users owner to www;
    ALTER table tig_nodes owner to www;
    ALTER table tig_pairs owner to www;

    ALTER TABLE tig_users
        ADD COLUMN django_user_id integer;

    ALTER TABLE tig_users
    ADD CONSTRAINT tig_users_django_user_fkey FOREIGN KEY (django_user_id)
      REFERENCES auth_user (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;


