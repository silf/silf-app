Database config
---------------

Postgresql need to have ``btree_gist`` extension installed.

Also you need to setup proper tigase integration
(see: :doc:`integration-tigase`).

