.. SILF WWW API documentation master file, created by
   sphinx-quickstart on Thu Feb 20 19:36:02 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SILF WWW API's documentation!
========================================

Contents:

.. toctree::
   :maxdepth: 2

   api
   database
   integration-tigase
   setting-options



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

