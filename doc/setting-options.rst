Settings
========

Tigase options
--------------

``TIGASE_SETUP_DB``

    Controls whether this application is integrating with Tigase.

    If set to ``True`` we have integration if ``False`` we don't.

``TIGASE_SERVER_DOMAIN``

    String after ``@`` in generated JIDs.

CORS options
------------

CORS allows resources on a web page to be requested from another domain. In this
application it allows Mercury to get experiments from the server.

``CORS_ORIGIN_WHITELIST``

    Specify a list of origin hostnames that are authorized to make a cross-site HTTP request.

    .. code-block:: python

        CORS_ORIGIN_WHITELIST = (
            'http://mercury-location:port',
        )

``CORS_ALLOW_CREDENTIALS``
    
    Should be set to ``True`` to allow cookies to be included in cross-site HTTP
    requests.

