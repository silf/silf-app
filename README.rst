================
SILF application
================


Development
===========

The `gitflow workflow <http://nvie.com/posts/a-successful-git-branching-model>`_ is used in this project, so allowed branch names are:

* master
* develop
* feature/<feature_name>
* release/<version>
* hotfix/<version>

We strongly recommend using `git-flow extension <https://github.com/nvie/gitflow>`_ with very useful set of `commands <http://danielkummer.github.io/git-flow-cheatsheet/>`_.

