# -*- coding: utf-8 -*-
import uuid

from django.core.exceptions import PermissionDenied

from django.views.generic import ListView, View
from django.views.generic.detail import DetailView
from django.utils.translation import ugettext_lazy
from .models import ExperimentSession, DataSeries

from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from silf.experiment_results.file_generator import FileGenerator
import json
from silf.experiment_results.serializers import \
    DataSeriesSerializer, ExperimentSessionSerializer


class ExperimentSeriesViewset(ModelViewSet):
    permission_classes = (DjangoModelPermissionsOrAnonReadOnly,)

    model = DataSeries
    serializer_class = DataSeriesSerializer
    queryset = DataSeries.objects.all().order_by('session__experiment', 'session')
    lookup_field = 'uuid'

    def get_queryset(self):
        qs = super(ExperimentSeriesViewset, self).get_queryset()
        user = self.request.user
        if self.request.query_params.get('show_all'):
            if not user.has_perm('experiment_results.see_all_experiment_sessions'):
                raise PermissionDenied()
        else:
            qs = qs.filter(session__participants__django_user__id__exact=user.pk)
        if 'experiment' in self.kwargs:
            qs = qs.filter(session__experiment__codename=self.kwargs['experiment'])
        if 'session_uuid' in self.kwargs:
            qs = qs.filter(session__uuid=self.kwargs['session_uuid'])

        return qs


class ExperimentSessionViewset(ModelViewSet):

    permission_classes = (DjangoModelPermissionsOrAnonReadOnly,)

    model = ExperimentSession
    serializer_class = ExperimentSessionSerializer
    queryset = ExperimentSession.objects.all().order_by('experiment')
    lookup_field = 'uuid'
    lookup_url_kwarg = 'uuid'

    def get_queryset(self):
        qs = super(ExperimentSessionViewset, self).get_queryset()
        exp = self.request.GET.get('experiment', None)

        if exp:
            qs = qs.filter(experiment__codename=exp)
        return qs


class UserDataSeriesList(ListView):
    template_name = "silf/experiment_results/data_series.html"

    def get_queryset(self):
        return DataSeries.objects.filter(
            session__participants__django_user__id__exact=self.request.user.id).order_by(
                'session__experiment__display_name')


class DownloadDataSeries(DetailView):

    queryset = DataSeries.objects.all()

    pk_url_kwarg = 'data_series'
    slug_url_kwarg = 'uuid'
    slug_field = 'uuid'

    def get_object(self, queryset=None):
        slug = self.kwargs.get(self.slug_url_kwarg, None)
        if slug is not None:
            try:
                self.kwargs[self.slug_url_kwarg] = uuid.UUID(hex=slug).urn
            except ValueError as e:
                raise DataSeries.DoesNotExist()
        return super(DownloadDataSeries, self).get_object(queryset)

    def render_to_response(self, context, **response_kwargs):

        data_series = self.object

        data = json.loads(data_series.data)

        file_generator = FileGenerator()
        format  = self.request.GET.get('format', 'csv')
        function = file_generator.format_function.get(format, None)
        if function is None:
            return HttpResponse(ugettext_lazy('Wrong file format'), status=400)

        filename = data_series.name + '.' + format
        response = HttpResponse()
        response['Content-Disposition'] = 'attachment; filename="' + filename + '"'
        buffer = function(data)
        response.write(buffer)
        return response
