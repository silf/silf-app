import csv
import xlwt
from numpy import savetxt
from io import StringIO


class FileGenerator:

    def get_values(self, data):
        keys = []
        values = []
        max_len = 0
        for key, value in data.items():
            v = value['value']
            if isinstance(v, list):
                max_len = max(max_len, len(v))
                if isinstance(v[0], list):
                    keys.append(key + 'x')
                    keys.append(key + 'y')
                else:
                    keys.append(key)
            else:
                keys.append(key)

        for i in range(max_len):
            l = []
            for key, value in data.items():
                v = value['value']
                if isinstance(v, list):
                    if isinstance(v[0], list):
                        if len(v) <= i:
                            for j in range(len(v[0])):
                                l.append('')
                        else:
                            l = l + v[i]
                    else:
                        if len(v) <= i:
                            l.append('')
                        else:
                            l.append(v[i])
                else:
                    if i > 0:
                        l.append('')
                    else:
                        l.append(v)

            values.append(l)

        return (keys, values)

    def get_csv(self, data):
        buffer = StringIO()
        (keys, values) = self.get_values(data)
        csvWriter = csv.writer(buffer)
        csvWriter.writerow(keys)
        for line in values:
            csvWriter.writerow(line)
        csv_file = buffer.getvalue()
        buffer.close()
        return csv_file

    def get_xls(self, data):
        buffer = StringIO()
        xls = xlwt.Workbook(encoding="utf-8")
        sh = xls.add_sheet("Results")
        (keys, values) = self.get_values(data)
        for (col, header) in enumerate(keys):
            sh.write(0, col, header)
        for (row, line) in enumerate(values):
            for (col, val) in enumerate(line):
                sh.write(row+1, col, val)
        xls.save(buffer)
        xls_file = buffer.getvalue()
        buffer.close()
        return xls_file

    def get_dat(self, data):
        buffer = StringIO()
        (keys, values) = self.get_values(data)
        savetxt(buffer, values, delimiter='\t', fmt='%s')
        dat_file = buffer.getvalue()
        buffer.close()
        return dat_file

    def __init__(self):
        self.format_function = {
            'csv': self.get_csv,
            'xls': self.get_xls,
            'dat': self.get_dat,
        }

    @property
    def format_function(self):
        return self._format_function

    @format_function.setter
    def format_function(self, value):
        self._format_function = value
