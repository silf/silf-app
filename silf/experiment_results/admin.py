# -*- coding: utf-8 -*-

from django.contrib import admin
from .models import DataSeries, ExperimentSession

admin.site.register(DataSeries)
admin.site.register(ExperimentSession)
