# -*- coding: utf-8 -*-
from django.urls import reverse

from django.db import models
from django.utils.translation import ugettext_lazy


class ExperimentSession(models.Model):
    experiment = models.ForeignKey("core.Experiment", related_name="exp_session",
                                   null=False, blank=False, on_delete=models.CASCADE)
    start_date = models.DateTimeField(auto_now_add=True)
    participants = models.ManyToManyField("tigase.TigaseUser")
    name = models.CharField(max_length=100)
    uuid = models.TextField(unique=True)

    class Meta:

        permissions = (
            ('see_all_experiment_sessions', ugettext_lazy("Can see all saved experiment sessions")),
        )


class DataSeries(models.Model):
    data = models.TextField()
    name = models.CharField(max_length=100)
    session = models.ForeignKey(ExperimentSession, related_name="series",
                                null=False, blank=False, on_delete=models.CASCADE)
    uuid = models.TextField(unique=True)

    def get_absolute_url(self):
        return reverse('download-data-series', kwargs={'data_series': self.pk})

