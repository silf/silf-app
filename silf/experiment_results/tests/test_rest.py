# coding=utf-8
import unittest
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.test import APIClient
from rest_framework.authtoken.models import Token
from django.urls import reverse
from django.conf import settings
from django.contrib.auth.models import User


class RestAPITestCase(APITestCase):

    # TODO: Fixtures are broken
    # fixtures = ['users_test', 'token_test', 'exp_test', 'tig_users_test']

    def auth_client(self):
        token = Token.objects.get(user__username='athena')
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        return self.client

    @unittest.skip(u"Fixtury są zepsute")
    def test_create_session(self):
        from silf.tigase.models import TigaseUser
        client = self.auth_client()
        data = {
            'uuid': 'urn:uuid:89721762-2c56-42ca-8ea4-0bffc2f11049',
            'name': 'example-averages',
            'participants': ['user$test_admin@host'],
            'experiment': 'example-averages',
        }
        url = reverse('session-create-list')
        response = client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data, data)

    @unittest.skip(u"Fixtury są zepsute")
    def test_update_session(self):
        client = self.auth_client()
        data = {
            'uuid': 'urn:uuid:89721762-2c56-42ca-8ea4-0bffc2f11049',
            'name': 'example-averages',
            'participants': ['user$test_admin@host'],
            'experiment': 'example-averages',
        }
        url = reverse('session-create-list')
        client.post(url, data)

        uuid = 'urn:uuid:89721762-2c56-42ca-8ea4-0bffc2f11049'
        data = {
            'participants': ['user$test@host'],
        }
        url = reverse('update-session-detail', args=(uuid,))
        response = client.patch(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['participants'], data['participants'])

    @unittest.skip(u"Fixtury są zepsute")
    def test_create_series(self):
        client = self.auth_client()
        data = {
            'uuid': 'urn:uuid:89721762-2c56-42ca-8ea4-0bffc2f11049',
            'name': 'example-averages',
            'participants': ['user$test_admin@host'],
            'experiment': 'example-averages',
        }
        url = reverse('session-create-list')
        client.post(url, data)

        session_uuid = 'urn:uuid:89721762-2c56-42ca-8ea4-0bffc2f11049'
        data = {
            'data': '{"chart": {"value": [[7, 7], [10, 28], [13, 32], [16, 7]],' +
                    '"pragma": "replace"}}',
            'name': 'Series1',
            'session': session_uuid,
            'uuid': 'urn:uuid:2f748bf3-7e79-4d7f-a66d-e12b1ceaba3d',
        }
        url = reverse('data-create-list')
        response = client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data, data)
