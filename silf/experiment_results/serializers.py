# -*- coding: utf-8 -*-
from django.core.exceptions import ObjectDoesNotExist
from django.utils.encoding import smart_text
from rest_framework import serializers
from silf.experiment_results import models as experiment_results_models
from silf.tigase import models as tigase_models
from silf.core import models as core_models
from silf.core.serializers import ExperimentSerializer


class ReadExperimentSessionSerializer(serializers.ModelSerializer):
    participants = serializers.RelatedField(many=True, read_only=True)

    experiment_session_id = serializers.CharField(source="uuid")

    class Meta:
        model = experiment_results_models.ExperimentSession
        depth = 1
        fields = ('experiment_session_id', 'name', 'participants', 'experiment')


class DataSeriesSerializer(serializers.ModelSerializer):
    experiment_session_id = serializers.SlugRelatedField(
        many=False, read_only=False, slug_field='uuid', source='session', queryset=experiment_results_models.ExperimentSession.objects.all())
    download_url = serializers.CharField(source='get_absolute_url', read_only=True)

    series_id = serializers.CharField(source="uuid")

    class Meta:
        model = experiment_results_models.DataSeries
        depth = 1
        fields = ('series_id', 'experiment_session_id', 'name', 'data', 'download_url')


def get_participants(participants_jid):

    if len(participants_jid) == 0:
        return []

    where_list = ["lower(user_id) LIKE lower(%s)"] * len(participants_jid)
    where_str = " OR ".join(where_list)
    participants_list = list(tigase_models.TigaseUser.objects.extra(
        where=[where_str], params=participants_jid).values('user_id'))
    return [el['user_id'] for el in participants_list]


class SessionMixin(object):

    def from_native(self, data, files=None):
        """
        Makes searching of participants case-insensitive and returns list of
        appropriate user_ids from database.
        """
        if data is not None:
            participants = data.getlist('participants')
            data = data.copy()
            participants_list = get_participants(participants)
            data.setlist('participants', participants_list)

        return super(SessionMixin, self).from_native(data, files)



class JIDRelatedField(serializers.SlugRelatedField):

    def to_internal_value(self, data):

        lookup = self.slug_field + "__iexact"

        try:
            return self.get_queryset().get(**{lookup: data})
        except ObjectDoesNotExist:
            self.fail('does_not_exist', slug_name=self.slug_field, value=smart_text(data))
        except (TypeError, ValueError):
            self.fail('invalid')


class ExperimentSessionSerializer(SessionMixin, serializers.ModelSerializer):

    participants = JIDRelatedField(
        many=True,
        read_only=False,
        slug_field='user_id',
        queryset=tigase_models.TigaseUser.objects.all()
    )

    experiment_session_id = serializers.CharField(source="uuid")

    experiment = serializers.SlugRelatedField(
        read_only=False,
        slug_field="codename",
        queryset=core_models.Experiment.objects.all()
    )

    class Meta:
        model = experiment_results_models.ExperimentSession
        depth = 0
        fields = ('experiment_session_id', 'experiment', 'participants', 'name')

