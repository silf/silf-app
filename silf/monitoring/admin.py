# -*- coding: utf-8 -*-

from django.contrib import admin
from silf.monitoring.models import Monitoring

admin.site.register(Monitoring)
