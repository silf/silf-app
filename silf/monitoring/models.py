# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy

from silf.experiment_results import models as experiment_results_models


EVENT_TYPE_CHOICES = (
    ('server_in', ugettext_lazy('Server in')),
    ('server_out', ugettext_lazy('Server out')),
    ('exp_start', ugettext_lazy('Start of experiment')),
    ('exp_end', ugettext_lazy('End of experiment')),
    ('series_start', ugettext_lazy('Start of series')),
    ('series_end', ugettext_lazy('End of series')),
)

ADDITIONAL_DATA_CHOICES = (
    ('reservation_end', ugettext_lazy('End of reservation')),
    ('user_ended', ugettext_lazy('Ended by user')),
    ('exp_ended', ugettext_lazy('Ended by experiment')),
    ('sess_ended_exp', ugettext_lazy('Session ended by experiment')),
    ('sess_ended_ath', ugettext_lazy('Session ended by athena')),
    ('sess_ended_usr', ugettext_lazy('Session ended by user')),
    ('timeout', ugettext_lazy('Timeout')),
    ('experiment_disconnected', ugettext_lazy('Experiment disconnected')),
    ('user_disconnected', ugettext_lazy('User disconnected')),
)


class Monitoring(models.Model):

    experiment = models.ForeignKey("core.Experiment", null=False, blank=False, on_delete=models.CASCADE)
    date = models.DateTimeField(null=False, blank=False, db_index=True)
    event_type = models.CharField(max_length=20, choices=EVENT_TYPE_CHOICES)
    additional_data = models.CharField(max_length=40, null=True, blank=True,
                                       choices=ADDITIONAL_DATA_CHOICES)
    session_uuid = models.TextField(null=True, blank=True)
    series_uuid = models.TextField(null=True, blank=True)

    def get_session(self):
        # ExperimentSession object with uuid=session_uuid may not exist yet -
        # Monitoring object can be created before ExperimentSession object.
        if self.session_uuid:
            session = experiment_results_models.ExperimentSession.objects.get(uuid=self.session_uuid)
            return session
        else:
            return None

    def get_series(self):
        # DataSeries object with uuid=series_uuid may not exist yet - Monitoring
        # object can be created before DataSeries object.
        if self.series_uuid:
            series = experiment_results_models.DataSeries.objects.get(uuid=self.series_uuid)
            return series
        else:
            return None
