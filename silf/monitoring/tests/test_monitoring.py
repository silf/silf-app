from unittest import skip
from django.test import TestCase

from silf.monitoring.serializers import MonitoringSerializer
from silf.monitoring.models import Monitoring
from silf.core.models import Experiment
import datetime
import pytz


class MonitoringTestCase(TestCase):

    def setUp(self):
        self.monitoring_serializer = MonitoringSerializer()
        self.experiment = Experiment.objects.create(codename='exp',
                                                    display_name='experiment')
        self.experiment_2 = Experiment.objects.create(codename='exp-2',
                                                      display_name='experiment-2')
        Monitoring.objects.create(experiment=self.experiment,
                                  date='2014-07-15T09:00:00+0000',
                                  event_type='server_in')
        Monitoring.objects.create(experiment=self.experiment,
                                  date='2014-07-15T09:05:00+0000',
                                  event_type='exp_start')
        Monitoring.objects.create(experiment=self.experiment,
                                  date='2014-07-15T09:06:00+0000',
                                  event_type='exp_stop')
        Monitoring.objects.create(experiment=self.experiment,
                                  date='2014-07-15T09:10:00+0000',
                                  event_type='server_out')
        Monitoring.objects.create(experiment=self.experiment,
                                  date='2014-07-15T09:12:00+0000',
                                  event_type='server_in')
        Monitoring.objects.create(experiment=self.experiment,
                                  date='2014-07-15T09:15:00+0000',
                                  event_type='exp_start')
        Monitoring.objects.create(experiment=self.experiment,
                                  date='2014-07-15T09:16:00+0000',
                                  event_type='series_start')
        Monitoring.objects.create(experiment=self.experiment,
                                  date='2014-07-15T09:17:00+0000',
                                  event_type='series_stop')
        Monitoring.objects.create(experiment=self.experiment,
                                  date='2014-07-15T09:18:00+0000',
                                  event_type='series_start')
        Monitoring.objects.create(experiment=self.experiment,
                                  date='2014-07-15T09:19:00+0000',
                                  event_type='series_stop')
        Monitoring.objects.create(experiment=self.experiment,
                                  date='2014-07-15T09:20:00+0000',
                                  event_type='exp_stop')
        Monitoring.objects.create(experiment=self.experiment,
                                  date='2014-07-15T09:25:00+0000',
                                  event_type='exp_start')
        Monitoring.objects.create(experiment=self.experiment,
                                  date='2014-07-15T09:26:00+0000',
                                  event_type='series_start')
        Monitoring.objects.create(experiment=self.experiment,
                                  date='2014-07-15T09:27:00+0000',
                                  event_type='series_stop')
        Monitoring.objects.create(experiment=self.experiment,
                                  date='2014-07-15T09:28:00+0000',
                                  event_type='exp_stop')
        Monitoring.objects.create(experiment=self.experiment,
                                  date='2014-07-15T09:30:00+0000',
                                  event_type='server_out')
        Monitoring.objects.create(experiment=self.experiment_2,
                                  date='2014-05-09T15:30:00+0000',
                                  event_type='server_in')
        Monitoring.objects.create(experiment=self.experiment_2,
                                  date='2014-05-09T15:31:00+0000',
                                  event_type='exp_start')
        Monitoring.objects.create(experiment=self.experiment_2,
                                  date='2014-05-09T15:35:00+0000',
                                  event_type='series_start')

    def test_exp_status(self):
        self.assertEqual(self.monitoring_serializer.get_exp_status(
            Monitoring.objects.get(experiment=self.experiment,
                                   date='2014-07-15T09:30:00+0000',
                                   event_type='server_out')), 'Offline')
        self.assertEqual(self.monitoring_serializer.get_exp_status(
            Monitoring.objects.get(experiment=self.experiment_2,
                                   date='2014-05-09T15:35:00+0000',
                                   event_type='series_start')), 'Online')

    @skip("Failing (and we know it)")
    def test_uptime(self):
        self.assertEqual(self.monitoring_serializer.get_uptime(
            Monitoring.objects.get(experiment=self.experiment,
                                   date='2014-07-15T09:30:00+0000',
                                   event_type='server_out')), datetime.timedelta(minutes=28))
        self.assertEqual(self.monitoring_serializer.get_uptime(
            Monitoring.objects.get(experiment=self.experiment_2,
                                   date='2014-05-09T15:35:00+0000',
                                   event_type='series_start'),
            datetime.datetime(2014, 5, 9, 20, 30, 0, 0, pytz.utc)), datetime.timedelta(hours=5))

    @skip("Failing (and we know it)")
    def test_get_amount_of_exp_sessions(self):
        self.assertEqual(self.monitoring_serializer.get_amount_of_exp_sessions(
            Monitoring.objects.get(experiment=self.experiment,
                                   date='2014-07-15T09:30:00+0000',
                                   event_type='server_out'),
            datetime.datetime(2014, 7, 15, 10, 0, 0, 0, pytz.utc)), 3)
        self.assertEqual(self.monitoring_serializer.get_amount_of_exp_sessions(
            Monitoring.objects.get(experiment=self.experiment,
                                   date='2014-07-15T09:30:00+0000',
                                   event_type='server_out'),
            datetime.datetime(2014, 5, 15, 10, 0, 0, 0, pytz.utc)), 0)
        self.assertEqual(self.monitoring_serializer.get_amount_of_exp_sessions(
            Monitoring.objects.get(experiment=self.experiment_2,
                                   date='2014-05-09T15:30:00+0000',
                                   event_type='server_in'),
            datetime.datetime(2014, 5, 9, 20, 0, 0, 0, pytz.utc)), 1)

    @skip("Failing (and we know it)")
    def test_get_amount_of_series_in_exp_session(self):
        self.assertEqual(self.monitoring_serializer.get_amount_of_series_in_exp_session(
            Monitoring.objects.get(experiment=self.experiment,
                                   date='2014-07-15T09:30:00+0000',
                                   event_type='server_out')), None)
        self.assertEqual(self.monitoring_serializer.get_amount_of_series_in_exp_session(
            Monitoring.objects.get(experiment=self.experiment,
                                   date='2014-07-15T09:05:00+0000',
                                   event_type='exp_start')), 0)
        self.assertEqual(self.monitoring_serializer.get_amount_of_series_in_exp_session(
            Monitoring.objects.get(experiment=self.experiment_2,
                                   date='2014-05-09T15:31:00+0000',
                                   event_type='exp_start')), 1)
