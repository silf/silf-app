# coding=utf-8
import unittest
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.test import APIClient
from rest_framework.authtoken.models import Token
from django.urls import reverse
from silf.monitoring.models import Monitoring
import datetime
import pytz


class RestAPITestCase(APITestCase):
    # TODO: Fix fixtures
    # fixtures = ['users_test', 'token_test', 'exp_test']

    def auth_client(self):
        token = Token.objects.get(user__username='athena')
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        return self.client

    @unittest.skip(u"Fixtury są zepsute")
    def test_create_monitoring(self):
        client = self.auth_client()
        data = {
            'date': datetime.datetime(2014, 7, 31, 8, 30, 0, 0, pytz.utc),
            'event_type': 'exp_start',
        }
        url = reverse('monitoring-create-list', args=('example-averages',))
        response = client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Monitoring.objects.filter(experiment='example-averages',
                                                   date=datetime.datetime(
                                                       2014, 7, 31, 8, 30, 0, 0, pytz.utc),
                                                   event_type='exp_start').exists(), True)
