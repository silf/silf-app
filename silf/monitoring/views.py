# -*- coding: utf-8 -*-

from rest_framework.viewsets import GenericViewSet, ReadOnlyModelViewSet, \
    ModelViewSet
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from silf.core.models import Experiment
from .models import Monitoring
from .serializers import MonitoringSerializer, MonitoringSaveSerializer


class MonitoringMixin(GenericViewSet):

    model = Monitoring
    queryset = Monitoring.objects.all().order_by("experiment", "date")
    serializer_class = MonitoringSerializer
    permission_classes = (DjangoModelPermissionsOrAnonReadOnly,)

    def get_queryset(self):
        qs = super(MonitoringMixin, self).get_queryset()
        if 'experiment' in self.kwargs:
            qs = qs.filter(experiment__codename=self.kwargs['experiment'])
        if 'event_type' in self.kwargs:
            qs = qs.filter(event_type=self.kwargs['event_type'])
        return qs


class MonitoringSaveMixin(MonitoringMixin):

    serializer_class = MonitoringSaveSerializer

    def get_queryset(self):
        """
        Raises Http404 if experiment does not exist.
        """
        get_object_or_404(Experiment, codename=self.kwargs['experiment'])
        return super(MonitoringSaveMixin, self).get_queryset()


class MonitoringSearch(MonitoringMixin, ReadOnlyModelViewSet):

    pass


class MonitoringCreate(MonitoringSaveMixin, ModelViewSet):

    pass
