# -*- coding: utf-8 -*-

from rest_framework import serializers
from silf.monitoring import models as monitoring_models
from silf.core import models as core_models
import datetime
import pytz


class MonitoringSerializer(serializers.ModelSerializer):

    # amount_of_exp_sessions = serializers.SerializerMethodField(
    #     'get_amount_of_exp_sessions')
    # amount_of_series_in_exp_session = serializers.SerializerMethodField(
    #     'get_amount_of_series_in_exp_session')
    exp_status = serializers.SerializerMethodField('get_exp_status')
    # uptime = serializers.SerializerMethodField('get_uptime')

    class Meta:
        model = monitoring_models.Monitoring
        depth = 1
        fields = ('id', 'experiment', 'date', 'event_type', 'additional_data',
                  'session_uuid', 'series_uuid',
                  # 'amount_of_exp_sessions',
                  # 'amount_of_series_in_exp_session',
                  'exp_status',
                  # 'uptime'
                  )

    # def get_amount_of_exp_sessions(self, obj,
    #                                end_day=datetime.datetime.now(pytz.utc),
    #                                time_period=30):
    #     start_day = end_day - datetime.timedelta(time_period)
    #     monitorings = Monitoring.objects.all()
    #     monitorings = monitorings.filter(experiment=obj.experiment,
    #                                      event_type='exp_start',
    #                                      date__range=(start_day, end_day))
    #     return monitorings.count()
    #
    # def get_amount_of_series_in_exp_session(self, obj):
    #     if obj.event_type != 'exp_start':
    #         return None
    #     else:
    #         monitorings = Monitoring.objects.all()
    #         next_obj_list = monitorings.filter(experiment=obj.experiment,
    #                                            event_type='exp_start',
    #                                            date__gt=obj.date).order_by(
    #                                                'date')[:1]
    #         if not next_obj_list:
    #             next_obj_date = datetime.datetime.now(pytz.utc)
    #         else:
    #             next_obj_date = next_obj_list[0].date
    #         monitorings = monitorings.filter(experiment=obj.experiment,
    #                                          event_type='series_start',
    #                                          date__range=(obj.date,
    #                                                       next_obj_date))
    #         return monitorings.count()

    def get_exp_status(self, obj):
        monitorings = monitoring_models.Monitoring.objects.all()
        last_exp_start = monitorings.filter(experiment=obj.experiment,
                                            event_type='exp_start').order_by(
                                                '-date')[:1]
        if not last_exp_start:
            return "Offline"
        else:
            monitorings = monitorings.filter(experiment=obj.experiment,
                                             event_type__in=['exp_end',
                                                             'server_out'],
                                             date__gte=last_exp_start[0].date)
            if monitorings:
                return "Offline"
            else:
                return "Online"

    # def get_uptime(self, obj,
    #                end_day=datetime.datetime.now(pytz.utc),
    #                time_period=30):
    #     start_day = end_day - datetime.timedelta(time_period)
    #     monitorings = Monitoring.objects.all()
    #     monitorings = monitorings.filter(experiment=obj.experiment,
    #                                      event_type__in=['server_in', 'server_out'],
    #                                      date__range=(start_day, end_day)
    #                                      ).order_by('date')
    #     if not monitorings:
    #         return 0
    #     else:
    #         monitoring_list = list(monitorings)
    #         uptime = datetime.timedelta(0)
    #         if monitoring_list[0].event_type == "server_out":
    #             m = Monitoring(date=start_day)
    #             monitoring_list = [m] + monitoring_list
    #         if monitoring_list[-1].event_type == "server_in":
    #             m = Monitoring(date=end_day)
    #             monitoring_list.append(m)
    #         count = 0
    #         while (count <= len(monitoring_list)/2):
    #             uptime = uptime + (monitoring_list[count+1].date
    #                                - monitoring_list[count].date)
    #             count = count + 2
    #         return uptime


class MonitoringSaveSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        self.__restore_object(validated_data)

    def __restore_object(self, attrs, instance=None):
        instance = super(MonitoringSaveSerializer, self).restore_object(
            attrs, instance)
        instance.experiment = core_models.Experiment.objects.get(
            codename=self.kwargs['experiment'])
        return instance

    @property
    def kwargs(self):
        return self.context['view'].kwargs

    class Meta:
        model = monitoring_models.Monitoring
        fields = ('id', 'date', 'event_type', 'additional_data', 'session_uuid',
                  'series_uuid')
