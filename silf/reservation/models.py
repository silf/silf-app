# -*- coding: utf-8 -*-

from django.core.exceptions import ValidationError
from django.db import models, transaction, DatabaseError
from django.db.transaction import atomic
import datetime
from django.utils.translation import ugettext_lazy

try:
    from psycopg2 import DatabaseError as psyDbError
except ImportError:
    psyDbError = DatabaseError
from rest_framework.exceptions import APIException


class InvalidReservationError(APIException):
    pass


class OverlapingReservationError(InvalidReservationError):
    status_code = 409
    detail = default_detail = "Overlapping"


class StartDateOlderThanEndDate(InvalidReservationError):
    status_code = 406
    detail = default_detail = "Start older than end"


class TooLongReservation(InvalidReservationError):
    status_code = 406
    detail = default_detail = "Too long"


class Reservation(models.Model):

    user = models.ForeignKey("auth.User", related_name="silf_reservations", null=False, blank=False, on_delete=models.CASCADE)
    experiment = models.ForeignKey("core.Experiment", related_name="reservations", null=False, blank=False, on_delete=models.CASCADE)
    start_date = models.DateTimeField(null=False, blank=False, db_index=True)
    end_date = models.DateTimeField(null=False, blank=False)

    @property
    def time_on_server(self):
        return datetime.datetime.utcnow()

    @property
    def jid(self):
        from silf.tigase import models as tigase_models
        return tigase_models.TigaseUser.objects.get_jid_for_django_user(self.user)

    @jid.setter
    def jid(self, val):
        if val != self.jid:
            raise ValueError(ugettext_lazy("Invalid jid for user {}".format(self.user.username)))

    def __str__(self):
        return "Reservation(user={}, experiment={}, start_date={}, end_date={})".format(self.user.username,
                                                                                        self.experiment.codename,
                                                                                        self.start_date, self.end_date)

    # "Czas rezerwacji wynosi {}s podczas gdy dla eksperymentu {} "
    #             "może on wynosić maksymalnie {}min"

    def clean(self):
        if not self.experiment:
            return
        duration = self.end_date - self.start_date

        if duration.total_seconds() < datetime.timedelta(seconds=0).total_seconds():
            raise StartDateOlderThanEndDate()

        if duration > self.experiment.max_reservation_length:
            raise TooLongReservation(
                ugettext_lazy("Reservation time is {sec} seconds, while for experiment"
                              " {exp} maximal time is {max} seconds").format(
                    sec=duration.total_seconds(), exp=self.experiment.display_name,
                    max=self.experiment.max_reservation_length.total_seconds()/60.0)
            )

    def save(self, *args, **kwargs):
        with atomic():
            try:
                self.full_clean()
            except ValidationError as e:
                raise TooLongReservation(*e.args)

            try:
                # Filter below counts reservations in DB that overlap with reservation we
                # intend to save.
                # Reservations overlap only if start of new reservation comes before end of
                # already present reservation AND end of new reservation comes after start of
                # said reservation. ie. it catches all four cases below:
                #
                #           start        end
                #             |----NEW----|
                #       |---1----|     |---3---|
                #                 |--2--|
                #       |-----------4------------|
                #
                overlapping_count = Reservation.objects.filter(end_date__gt=self.start_date,
                                                               start_date__lt=self.end_date,
                                                               experiment=self.experiment).count()

                if overlapping_count != 0:
                    raise OverlapingReservationError("{} overlaps with {} reservations".format(self, overlapping_count))

                super(Reservation, self).save(*args, **kwargs)
            except (DatabaseError, psyDbError) as e:
                if 'reservation_dates' in e.args[0]:
                    raise StartDateOlderThanEndDate(*e.args)
                raise
