# Create your views here.
from __future__ import unicode_literals
import datetime
from django.contrib.auth import get_user_model
from django.db.models import Q

import django_filters

from rest_framework.generics import ListAPIView, get_object_or_404
from rest_framework.permissions import SAFE_METHODS, IsAuthenticatedOrReadOnly, BasePermission, DjangoModelPermissionsOrAnonReadOnly
from rest_framework.viewsets import ViewSet, ModelViewSet, ReadOnlyModelViewSet, GenericViewSet
from silf.core.models import Experiment
from silf.reservation.forms import RecentReservationForm
from silf.reservation.models import Reservation
from silf.reservation.serializers import SaveReservationSerializer, ReadReservationSerializer

from rest_framework.routers import Route, SimpleRouter
from silf.reservation.utils import now_as_utc


class ReservationPermission(BasePermission):
    """
    The request is authenticated as a user, or is a read-only request.
    """

    def __init__(self):
        self.internal = DjangoModelPermissionsOrAnonReadOnly()

    def has_object_permission(self, request, view, obj):

        if self.internal.has_permission(request, view):
            return True

        user = request.user
        return user.is_authenticated and obj.user == user


class ReservationFilterSet(django_filters.FilterSet):

    start_date_min = django_filters.DateTimeFilter(field_name="start_date", lookup_expr='gte')
    start_date_max = django_filters.DateTimeFilter(field_name="start_date", lookup_expr='lte')
    end_date_min = django_filters.DateTimeFilter(field_name="end_date", lookup_expr='gte')
    end_date_max = django_filters.DateTimeFilter(field_name="end_date", lookup_expr='lte')

    class Meta:
        model = Reservation
        fields = ['start_date_min', 'start_date_max', 'end_date_min', 'end_date_max']


class ReservationMixin(GenericViewSet):

    model = Reservation
    queryset = Reservation.objects.all().order_by("experiment", "start_date")
    serializer_class = ReadReservationSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, ReservationPermission)
    filter_class = ReservationFilterSet

    def get_queryset(self):
        qs = super(ReservationMixin, self).get_queryset()
        if 'experiment' in self.kwargs:
            qs = qs.filter(experiment__codename = self.kwargs['experiment'])
        if 'user' in self.kwargs:
            qs = qs.filter(user__username = self.kwargs['user'])
        if 'recent' in self.kwargs and self.kwargs['recent']:
            now = now_as_utc()
            current_reservation = Q(start_date__lte=now, end_date__gt=now)
            future_reservations = Q(start_date__gte=now)
            qs = qs.filter(current_reservation | future_reservations)
        # print(qs.query)
        return qs


class ReservationSaveMixin(ReservationMixin):

    serializer_class = SaveReservationSerializer

    def get_queryset(self):
        #Make it explode if user or experiment is invalid
        get_object_or_404(get_user_model(), username=self.kwargs['user'])
        get_object_or_404(Experiment, codename=self.kwargs['experiment'])
        return super(ReservationSaveMixin, self).get_queryset()


class ReservationSearch(ReservationMixin, ReadOnlyModelViewSet):
    pass


class ReservationCreate(ReservationSaveMixin, ModelViewSet):
    pass
    #def dispatch(self, request, *args, **kwargs):
    #    import pudb; pudb.set_trace();
    #    return super().dispatch(request, *args, **kwargs)
