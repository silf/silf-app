# -*- coding: utf-8 -*-
from django import forms


class RecentReservationForm(forms.Form):

    experiment = forms.CharField(required=True)