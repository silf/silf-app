# -*- coding: utf-8 -*-
from datetime import timedelta
import datetime

import pytz

from django.contrib.auth import get_user_model
from django.utils import timezone
from rest_framework import serializers
from rest_framework.fields import IntegerField
from silf.core.serializers import ExperimentSerializer

from silf.core import models as core_models
from silf.reservation import models as reservation_models

from silf.reservation.utils import now_as_utc


class DateTimeTzAwareField(serializers.DateTimeField):

    def to_representation(self, value):
        if timezone.is_aware(value):
            value = timezone.localtime(value, timezone=pytz.utc)
        return super(DateTimeTzAwareField, self).to_representation(value)


class NowField(DateTimeTzAwareField):

    def to_internal_value(self, value):
        if value == "now":
            return now_as_utc()
        return super(DateTimeTzAwareField, self).to_internal_value(value)


class UserSerializer(serializers.ModelSerializer):

    """
    >>> user = get_user_model()(username="Test")
    >>> UserSerializer(user).data == {'username': u'Test'}
    True
    """

    class Meta:
        model = get_user_model()
        fields = ['username']


class ReadReservationSerializer(serializers.ModelSerializer):

    """
    >>> from datetime import datetime
    >>> user = get_user_model()(username="Test")
    >>> experiment = core_models.Experiment(codename="exp", display_name="Experiment", max_reservation_length=timedelta(minutes=100))
    >>> reservation = reservation_models.Reservation(
    ...  user = user,
    ...  experiment = experiment,
    ...  start_date = datetime(2012, 1, 1),
    ...  end_date = datetime(2012, 1, 2)
    ... )
    >>> ReadReservationSerializer(reservation).data == {
    ...   'id': None,
    ...   'jid': u"user$Test",
    ...   'user': {'username': u'Test'},
    ...   'experiment': {'codename': u'exp', 'display_name': u'Experiment', 'max_reservation_length': 100},
    ...   'start_date': '2012-01-01T00:00:00',
    ...   'end_date': '2012-01-02T00:00:00'
    ... }
    True
    """

    jid = serializers.CharField(read_only=True)
    start_date = DateTimeTzAwareField()
    end_date = DateTimeTzAwareField()
    time_on_server = DateTimeTzAwareField(read_only=True)

    user = UserSerializer(read_only=True)

    def get_nested_field(self, model_field, related_model, to_many):
        if related_model == get_user_model():
            return UserSerializer(many=to_many)
        if related_model == core_models.Experiment:
            return ExperimentSerializer(many=to_many)
        return super(ReadReservationSerializer, self).get_nested_field(
            model_field, related_model, to_many)

    class Meta:
        model = reservation_models.Reservation
        depth = 1
        fields = ('id', 'jid', 'start_date', 'end_date', 'experiment', 'user', 'time_on_server')


class SaveReservationSerializer(serializers.ModelSerializer):
    start_date = NowField()
    end_date = DateTimeTzAwareField(read_only=True)
    reservation_length_minutes = IntegerField(min_value=0, write_only=True)

    def create(self, validated_data):
        return self.__restore_object(validated_data)

    def __restore_object(self, validated_data):
        reservation_length_minutes = validated_data.pop('reservation_length_minutes')
        #instance = super(SaveReservationSerializer, self).create(validated_data)
        instance = reservation_models.Reservation(**validated_data)

        instance.experiment = core_models.Experiment.objects.get(codename=self.kwargs['experiment'])
        instance.user = get_user_model().objects.get(username=self.kwargs['user'])
        instance.end_date = instance.start_date + timedelta(minutes=reservation_length_minutes)
        instance.save()
        return instance

    @property
    def kwargs(self):
        return self.context['view'].kwargs

    class Meta:
        model = reservation_models.Reservation
        depth = 0
        fields = ('id', 'start_date', 'reservation_length_minutes', 'end_date')
