��          �      L      �     �     �  
   �     �               ;     N     d  +   �     �     �  [   �     6     F     b  (   |     �  �  �     9     J     ^     j  "   {     �     �     �     �  *   �     )     9  b   I     �     �     �  /   �     ,     
                                                                                  	             Bad date format Choose experiment Experiment Hour for reservation Invalid jid for user {} Invalid reservation hour format Make a reservation Please fill the field Please give reservation date Reservation can't be for date in the future Reservation date Reservation time in minutes Reservation time is {sec} seconds, while for experiment {exp} maximal time is {max} seconds Reserve for now Select reservation duration Select reservation method To make a reservation you need to log in ^/reservation/$ Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-07-08 21:04+0300
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Zły format daty Wybierz eksperyment Eksperyment Wybierz godzinę Niepoprawny JID la użytkownika {} Niepoprawny format godziny Dokonaj rezerwacji Proszę wypełnić to pole Podaj datę rezerwacji Rezerwacja nie może być w przyszłości Data rezerwacji Czas rezerwacji Wybrano rezerwację na {sec} sekund, podczas gdy maksymalny czas reserwacji dla {exp} wynosi {max} Zarezerwuj 'na teraz' Wybierz długość rezerwacji  Wybierz metodę rezerwacji By wykonać rezerwację musisz się zalogować ^/rezerwacja/$ 