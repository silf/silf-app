'use strict';

angular.module('reservationServices', ['ngResource'])
    .factory('Reservation', ['$resource', '$http', '$window', ($resource, $http, $window) ->
        console.log('in reserv' + $window.location)
        # change content type of post requests
        defaults = $http.defaults.headers;
        defaults.post = defaults.post || {};
        defaults.post['Content-Type']='application/x-www-form-urlencoded';

        $resource(
            window.location.protocol + '//' + window.location.host + '/silf/api/reservation/user/:userName/experiment/:experimentName',
            {userName: '@userName', experimentName: '@expName'}
        );
    ]);

angular.module('experimentServices', ['ngResource'])
    .factory('ExpService', ['$resource', ($resource) ->
        $resource(
            window.location.protocol + '//' + window.location.host + '/silf/api/experiments'
        );
    ]);


angular.module('reservationApp', ['reservationServices', 'experimentServices', 'ui.bootstrap'])
    .controller('ReservationCtrl', ['$scope', '$timeout', 'Reservation', 'ExpService', 'datepickerPopupConfig', ($scope, $timeout, Reservation, ExpService, datepickerPopupConfig) ->

        $scope.possibleDurations = [];
        $scope.duration = null;
        $scope.experiments = [];
        $scope.experiment = null;
        $scope.startDate = null;
        $scope.startTime = null;
        $scope.errorMsg = null;
        $scope.successMsg = null;
        $scope.reservMode = 'DATE';
        expListReqResult = null;

        $scope.makeReservation = (reservForm) ->
            console.log("makeReservation invalid=" + reservForm.$invalid)
            $scope.successMsg = null;
            $scope.errorMsg = isDateAndTimeValid(reservForm, $scope.startDate, $scope.startTime)
            console.log("$scope.errorMsg: " + $scope.errorMsg + " invalid=" + reservForm.$invalid)
            if $scope.errorMsg.length > 0 || reservForm.$invalid
                return false;

            currentExp = $scope.experiment.codename;
            currentUser = document.getElementById('currentUser').value;

            reservationData = ""
            if $scope.reservMode == "DATE"
                reservationData = createReservationData(getDateWithTime($scope.startDate, $scope.startTime), $scope.duration)
            else
                reservationData = createReservationData("now", $scope.duration)
            Reservation.save({userName: currentUser, experimentName: currentExp},
                reservationData, onReservationSucess, onReservationFailure);

            return false;

        createReservationData = (startTime, duration) ->
            postDat = 'csrfmiddlewaretoken=' + document.getElementsByName('csrfmiddlewaretoken')[0].value + '&';
            startStr = "now"
            if startTime != "now"
                startStr = dateToRestFormat(startTime)
            postDat += 'start_date=' + encodeURIComponent(startStr) + '&';
            postDat += 'reservation_length_minutes=' + encodeURIComponent(duration);
            return postDat;

        # format date to 'DATETIME_FORMAT': '%Y-%m-%dT%H:%M:%S%z' ex: 2013-12-05T16:00:00+0100
        # for REST request
        dateToRestFormat = (dateObj) ->
            return dateObj.toISOString()

        onReservationSucess = (succ) ->
#            console.log('onReservationSucess');
#            console.log(succ)
            stDate = new Date(Date.parse(succ.start_date))
            sm = "Dokonano rezerwacja na " + stDate.toLocaleDateString() +
                " " + stDate.toLocaleTimeString() + " dla eksperymentu " + $scope.experiment.displayName;
            # wyczysc formularz
            $scope.startDate = null;
            $scope.startTime = null;
            $scope.reservMode = 'DATE';
            $scope.onReservModeChange()
            $scope.experiment = null;
            $scope.duration = null;
            $scope.possibleDurations = [];
            $scope.errorMsg = null;
            # ustaw na formularzu ze jest valid po wyczyszczeniu
            $scope.experimentReservation.$setPristine()

            $scope.successMsg = sm;

            return;

        onReservationFailure = (error) ->
            #TODO obiekty zbledami sa jakies dziwne - tablice znakow?!
            console.log(error)
            console.log('onReservationFailure');

            switch error.status
                when 409 then $scope.errorMsg = ['Nie można dokonać rezerwacji. W podanym czasie dokonano juz rezerwacji. ']
                when 406 then $scope.errorMsg = ['Rezerwacja jest za długa. Wybierz inny czas trwania rezerwacji.']
                #TODO ponizej mozna zrobic parsowanie error.data aby wyciagnac komunikaty bledow
                else $scope.errorMsg = ['Wystąpił błąd, twoja rezerwacja nie powiodła się. Spróbuj ponownie.']

            return;

        onExperimentsLoaded = () ->
            resList = []
            for exp in expListReqResult
                ne = {}
                ne.codename=exp.codename
                ne.displayName = exp.display_name
                ne.maxReservation = exp.max_reservation_length
#                console.log('max rezerwacja: ' + ne.maxReservation)
                resList.push(ne)
            $scope.experiments = resList

            return;

        onExperimentsLoadFailure = () ->
            $scope.errorMsg = ['Nie można załadować listy dostępnych eksperymentów. Przeładuj strone ponownie.']
            return;

        $scope.onExperimentChange = () ->
            durTab = [5,15,30]
            dur = 60
            while dur <= $scope.experiment.maxReservation
                durTab.push(dur)
                dur += 30
            $scope.possibleDurations = durTab
            return true

        isDateAndTimeValid = (reservForm, dateVal, timeStr) ->
            console.log(reservForm)
#            console.log(dateVal)
            errMsg = []
            if not document.getElementById('currentUser').value
                errMsg.push('Aby dokonać rezerwacji musisz być zalogowany')

            if $scope.reservMode == "DATE" and dateVal and timeStr
                cd = new Date()
                try
                    pd = getDateWithTime(dateVal, timeStr)
                    if pd.getTime() < cd.getTime()
                        console.log(pd)
                        console.log(cd)
                        reservForm.reservationTime.$setValidity('inPast', false)
                        reservForm.reservationDate.$setValidity('inPast', false)
                    else
                        reservForm.reservationTime.$setValidity('inPast', true)
                        reservForm.reservationDate.$setValidity('inPast', true)
                catch e
                    # ignore exception
                    console.log(e)

            return errMsg;

        getDateWithTime = (dateVal, timeStr) ->
            resultDate = new Date(dateVal.getTime())
            timeTab = timeStr.split(':')
            resultDate.setHours(timeTab[0])
            resultDate.setMinutes(timeTab[1])
            resultDate.setSeconds(0)
            resultDate.setMilliseconds(0)

            return resultDate

        $scope.onReservModeChange = () ->
            $("div.byDate").each(() ->
                if $scope.reservMode == "DATE"
                    $(this).show()
                else
                    $(this).hide()
            )
            if $scope.reservMode == "NOW"
                $scope.startDate = new Date();
                $scope.startTime = "00:00";
            else
                $scope.startDate = null;
                $scope.startTime = null;
                $scope.experimentReservation.reservationTime.$setPristine()

            return true;

        # date picker start
        datepickerPopupConfig.closeText = 'Zamknij'
        datepickerPopupConfig.currentText = 'Dziś'
        datepickerPopupConfig.toggleWeeksText = 'Tygodnie'
        datepickerPopupConfig.clearText = 'Wyczyść'
        datepickerPopupConfig.closeOnDateSelection = true
        # !!! dateFormat is also set in html file
        datepickerPopupConfig.dateFormat = 'dd/MM/yyyy'

        $scope.dateOptions = {
            'year-format': "'yyyy'",
            'starting-day': 1,
        };

        $scope.minDate = new Date();

        nowPlusMilis = (milisToAdd) ->
            dd = new Date()
            dd.setTime(dd.getTime() + milisToAdd)
            return dd

        # maximum reservation is 90 days in future
        $scope.maxDate = nowPlusMilis(1000*60*60*24*90)

        $scope.datePickerOpen = () ->
            if $scope.reservMode == "DATE"
                $timeout(() ->
                    $scope.datePickerOpened = true;
                );
            return true;

        # date picker END

        # load experiment list
        expListReqResult = ExpService.query([], onExperimentsLoaded, onExperimentsLoadFailure);

        return null;
    ]);
# nie rob nic ponizej - powyzszy return zamyka modul

formatDate = (dateObj) ->
    dateStr = ""
    if dateObj.getDate() < 10
        dateStr += "0"
    dateStr += dateObj.getDate() + "/"
    if (dateObj.getMonth() + 1) < 10
        dateStr += "0"
    dateStr += (dateObj.getMonth() + 1) + "/"
    return dateStr + dateObj.getFullYear();
