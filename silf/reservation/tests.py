"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from datetime import timedelta

from django.contrib.auth.models import User
from django.conf import settings
from django.core.exceptions import ValidationError
from django.urls import reverse
from django.test import TestCase, TransactionTestCase
from django.utils.datetime_safe import datetime
from django.utils.timezone import utc, activate, deactivate, get_current_timezone, is_aware
from mock import patch
from rest_framework.test import APIClient
from silf.core import models as core_models
from silf.reservation import models as models
from silf.reservation.serializers import NowField
from silf.reservation.utils import now_as_utc
import unittest


class BaseTestFixture(object):

    def setUp(self):
        self.user = User.objects.create(username="foo")
        self.user2 = User.objects.create(username="bar")
        self.user3 = User.objects.create(username="baz")
        self.admin = User.objects.create(username="admin", is_superuser=True)
        self.admin.set_password("foo")
        self.admin.save()
        self.experiment1 = core_models.Experiment.objects.create(codename="e1", display_name="e1", max_reservation_length=timedelta(days=10))
        self.experiment2 = core_models.Experiment.objects.create(codename="e2", display_name="e2", max_reservation_length=timedelta(days=10))

if 'postgresql' in settings.DATABASES['default']['ENGINE']:

    class TestReservationExclusionConstraint(BaseTestFixture, TransactionTestCase):

        def test_overlaping(self):
            models.Reservation.objects.create(user=self.user, experiment=self.experiment1, start_date=datetime(2012, 12, 1), end_date=datetime(2012, 12, 2))
            self.assertRaises(
              models.OverlapingReservationError,
              models.Reservation.objects.create,
              user=self.user, experiment=self.experiment1, start_date=datetime(2012, 12, 1), end_date=datetime(2012, 12, 2)
            )

        def test_partially_overlaping(self):
            models.Reservation.objects.create(user=self.user, experiment=self.experiment1, start_date=datetime(2012, 12, 1), end_date=datetime(2012, 12, 3))
            self.assertRaises(
              models.OverlapingReservationError,
              models.Reservation.objects.create,
              user=self.user, experiment=self.experiment1, start_date=datetime(2012, 12, 2), end_date=datetime(2012, 12, 4)
            )

        def test_partially_overlaping_2(self):
            models.Reservation.objects.create(user=self.user, experiment=self.experiment1, start_date=datetime(2012, 12, 1), end_date=datetime(2012, 12, 3))
            self.assertRaises(
              models.OverlapingReservationError,
              models.Reservation.objects.create,
              user=self.user, experiment=self.experiment1, start_date=datetime(2012, 11, 25), end_date=datetime(2012, 12, 2)
            )

        def test_partially_overlaping_3(self):
            models.Reservation.objects.create(user=self.user, experiment=self.experiment1, start_date=datetime(2012, 12, 1), end_date=datetime(2012, 12, 5))
            self.assertRaises(
              models.OverlapingReservationError,
              models.Reservation.objects.create,
              user=self.user, experiment=self.experiment1, start_date=datetime(2012, 12, 2), end_date=datetime(2012, 12, 3)
            )

        def test_overlaping_but_different_experiments(self):
            models.Reservation.objects.create(user=self.user, experiment=self.experiment1, start_date=datetime(2012, 12, 1), end_date=datetime(2012, 12, 3))
            models.Reservation.objects.create(user=self.user, experiment=self.experiment2, start_date=datetime(2012, 12, 1), end_date=datetime(2012, 12, 3))

        def test_end_date_greater_than_start_date(self):
            self.assertRaises(
                models.StartDateOlderThanEndDate,
                models.Reservation.objects.create,
                user=self.user, experiment=self.experiment1, end_date=datetime(2012, 12, 2), start_date=datetime(2012, 12, 4)
            )

        def test_partially_overlaping(self):
            models.Reservation.objects.create(user=self.user, experiment=self.experiment1, start_date=datetime(2012, 12, 1), end_date=datetime(2012, 12, 3))
            r = models.Reservation(user=self.user, experiment=self.experiment1, start_date=datetime(2012, 12, 2), end_date=datetime(2012, 12, 4))
            self.assertRaises(
              models.OverlapingReservationError,
              r.save
            )


class TestReservationMaxLength(BaseTestFixture, TestCase):

    def __create_too_long_reservation(self):
        # raise ValueError(self.experiment1.max_reservation_length)
        return models.Reservation(
                user=self.user, experiment=self.experiment1,
                start_date=datetime(2012, 12, 1), end_date=datetime(2012, 12, 13))

    def test_ok_duration(self):
        return models.Reservation(user=self.user, experiment=self.experiment1, start_date=datetime(2012, 12, 1), end_date=datetime(2012, 12, 3))

    def test_too_long_duration(self):
        with self.assertRaises(models.TooLongReservation):
            self.__create_too_long_reservation().save()

    def test_default_duration_ok(self):
        e = core_models.Experiment.objects.create(codename="e3", display_name="e3")
        models.Reservation.objects.create(
            user=self.user, experiment=e,
            start_date=datetime(2012, 12, 1, 12, 12),
            end_date=datetime(2012, 12, 1, 12, 15))

    def __create_too_long_reservation_for_default_time(self):
        e = core_models.Experiment.objects.create(codename="e3", display_name="e3")
        return models.Reservation(
                user=self.user, experiment=e,
                start_date=datetime(2012, 12, 1, 12, 12),
                end_date=datetime(2012, 12, 3, 13, 15))

    def test_default_duration_errror(self):
        r = self.__create_too_long_reservation_for_default_time()
        with self.assertRaises(models.TooLongReservation):
            r.save()

    def test_default_duration_validation_errror(self):
        r = self.__create_too_long_reservation_for_default_time()
        with self.assertRaises(models.TooLongReservation):
            r.full_clean()


class TestReservations(BaseTestFixture, TestCase):

    def setUp(self):
        super(TestReservations, self).setUp()
        self.client = APIClient()
        activate("Europe/Warsaw")
        self.r11 = models.Reservation.objects.create(user=self.user, experiment=self.experiment1, start_date=datetime(2012, 12, 1, tzinfo=utc), end_date=datetime(2012, 12, 3, tzinfo=utc))
        self.r21 = models.Reservation.objects.create(user=self.user, experiment=self.experiment2, start_date=datetime(2012, 12, 1, tzinfo=utc), end_date=datetime(2012, 12, 3, tzinfo=utc))
        self.r12 = models.Reservation.objects.create(user=self.user, experiment=self.experiment1, start_date=datetime(2012, 12, 4, tzinfo=utc), end_date=datetime(2012, 12, 6, tzinfo=utc))
        self.r22 = models.Reservation.objects.create(user=self.user, experiment=self.experiment2, start_date=datetime(2012, 12, 4, tzinfo=utc), end_date=datetime(2012, 12, 6, tzinfo=utc))
        self.r11 = models.Reservation.objects.create(user=self.user2, experiment=self.experiment1, start_date=datetime(2012, 11, 1, tzinfo=utc), end_date=datetime(2012, 11, 3, tzinfo=utc))
        self.r21 = models.Reservation.objects.create(user=self.user2, experiment=self.experiment2, start_date=datetime(2012, 11, 1, tzinfo=utc), end_date=datetime(2012, 11, 3, tzinfo=utc))

    def tearDown(self):
        deactivate()

    def test_reservation_list(self):
        response = self.client.get(reverse('reservation-list') + ".json")

        self.assertEqual(len(response.data), 6)
        experument_names = [r['experiment']['codename'] for r in response.data]
        self.assertIn('e2', experument_names)
        self.assertIn('e1', experument_names)
        user_names = [r['user']['username'] for r in response.data]
        self.assertIn('foo', user_names)
        self.assertIn('bar', user_names)

    def test_reservation_filtering_by_user_name(self):
        response = self.client.get(
            reverse('reservation-user-list', kwargs={'user': 'foo'}) + ".json")

        self.assertEqual(len(response.data), 4)
        experument_names = [r['experiment']['codename'] for r in response.data]
        self.assertIn('e2', experument_names)
        self.assertIn('e1', experument_names)
        user_names = [r['user']['username'] for r in response.data]
        self.assertIn('foo', user_names)
        self.assertNotIn('bar', user_names)

    def test_reservation_filtering_by_exp_name(self):
        response = self.client.get(
            reverse('reservation-experiment-list', kwargs={'experiment': 'e1'}) + ".json")

        self.assertEqual(len(response.data), 3)
        experument_names = [r['experiment']['codename'] for r in response.data]
        self.assertNotIn('e2', experument_names)
        self.assertIn('e1', experument_names)
        user_names=[r['user']['username'] for r in response.data]
        self.assertIn('foo', user_names)
        self.assertIn('bar', user_names)

    def test_reservation_filtering_by_exp_name_user(self):
        response = self.client.get(
            reverse('reservation-experiment-user-list', kwargs={'experiment': 'e1', 'user': 'foo'}) + ".json")

        self.assertEqual(len(response.data), 2)

    @patch("silf.reservation.views.now_as_utc")
    def test_recent_1(self, now_as_utc):

        now_as_utc.return_value = datetime(2012, 12, 7, tzinfo=utc)

        response = self.client.get(reverse('reservation-list', kwargs={"recent": "/recent"}) + ".json")

        self.assertEqual(len(response.data), 0)

    @patch("silf.reservation.views.now_as_utc")
    def test_recent_returns_current_experiment(self, now_as_utc):

        now_as_utc.return_value = datetime(2012, 12, 5, tzinfo=utc)

        response = self.client.get(reverse('reservation-list', kwargs={"recent": "/recent"}) + ".json")

        self.assertEqual(response.status_code, 200)

        self.assertEqual(len(response.data), 2)
        experiment_names = [r['experiment']['codename'] for r in response.data]
        self.assertIn('e2', experiment_names)
        self.assertIn('e1', experiment_names)

    @patch("silf.reservation.views.now_as_utc")
    def test_recent_returns_current_experiment_2(self, now_as_utc):

        now_as_utc.return_value = datetime(2012, 12, 5, tzinfo=utc)

        response = self.client.get(reverse('reservation-experiment-list', kwargs={"experiment": "e1", "recent": "/recent"}) + ".json")

        self.assertEqual(response.status_code, 200)

        self.assertEqual(len(response.data), 1)
        experiment_names = [r['experiment']['codename'] for r in response.data]
        self.assertNotIn('e2', experiment_names)
        self.assertIn('e1', experiment_names)

    @patch("silf.reservation.views.now_as_utc")
    def test_recent_returns_current_and_next_experiment_2(self, now_as_utc):

        now_as_utc.return_value = datetime(2012, 12, 2, tzinfo=utc)

        response = self.client.get(reverse('reservation-experiment-list', kwargs={"experiment": "e1", "recent": "/recent"}) + ".json")

        self.assertEqual(response.status_code, 200)

        self.assertEqual(len(response.data), 2)
        experiment_names = [r['experiment']['codename'] for r in response.data]
        self.assertNotIn('e2', experiment_names)
        self.assertIn('e1', experiment_names)

    def test_save_reservation_test_response(self):
        start_date = datetime(2012, 12, 12, 12, 12, tzinfo=utc)
        reservation_length_min = 120
        self.client.force_authenticate(self.admin)
        response = self.client.post(
            reverse("reservation-experiment-user-list",
                    kwargs={"experiment": "e1", "recent": "/recent", 'user': 'baz'}),
            data={"start_date": start_date.isoformat(), "reservation_length_minutes": reservation_length_min})

        data = response.data
        self.assertEqual(response.status_code, 201)

        self.assertEqual(data['start_date'], "2012-12-12T12:12:00+0000")
        self.assertEqual(data['end_date'], "2012-12-12T14:12:00+0000")

    def test_save_reservation_test_entity(self):
        start_date = datetime(2012, 12, 12, 12, 12, tzinfo=utc)
        reservation_length_min = 120
        end_date = start_date + timedelta(minutes=reservation_length_min)
        self.client.force_authenticate(self.admin)
        response = self.client.post(
            reverse("reservation-experiment-user-list",
                    kwargs={"experiment": "e1", "recent":"/recent", 'user': 'baz'}),
            data={"start_date": start_date.isoformat(), "reservation_length_minutes": reservation_length_min})

        self.assertEqual(response.status_code, 201)

        r = models.Reservation.objects.get(experiment__codename='e1', user__username = 'baz')
        self.assertEqual(r.start_date, start_date)
        self.assertEqual(r.end_date, end_date)

    @patch("silf.reservation.serializers.now_as_utc")
    def test_save_reservation_now(self, now_as_utc):
        now_as_utc.return_value = datetime(2012, 12, 12, 12, 12, tzinfo=utc)
        reservation_length_min = 120
        end_date = datetime(2012, 12, 12, 12, 12, tzinfo=utc) + timedelta(minutes=reservation_length_min)
        self.client.force_authenticate(self.admin)
        response = self.client.post(
            reverse("reservation-experiment-user-list",
                    kwargs={"experiment": "e1", "recent": "/recent", 'user': 'baz'}),
            data={"start_date": "now", "reservation_length_minutes": reservation_length_min})

        self.assertEqual(response.status_code, 201)

        r = models.Reservation.objects.get(experiment__codename='e1', user__username = 'baz')
        self.assertEqual(r.start_date, datetime(2012, 12, 12, 12, 12, tzinfo=utc))
        self.assertEqual(r.end_date, datetime(2012, 12, 12, 14, 12, tzinfo=utc))


# THIS needs refinement --- When we debug timezone please add proper tests

class TestReservationsTimes(BaseTestFixture, TestCase):

    def __get_datetime(self, hour, minute=0, seconds=0, tzinfo=None):
        if tzinfo is None:
            tzinfo = utc
        return datetime(2012, 12, 1, hour, minute, seconds, tzinfo=tzinfo)

    def setUp(self):
        super(TestReservationsTimes, self).setUp()
        self.client = APIClient()
        activate("Europe/Warsaw")

        self.r11 = models.Reservation.objects.create(user=self.user, experiment=self.experiment1, start_date=self.__get_datetime(12), end_date=self.__get_datetime(13))
        self.r21 = models.Reservation.objects.create(user=self.user, experiment=self.experiment1, start_date=self.__get_datetime(13, seconds=1), end_date=self.__get_datetime(14))

    @patch("silf.reservation.views.now_as_utc")
    def test_recent_view(self, now_as_utc):

        now_as_utc.return_value = self.__get_datetime(13, 0, tzinfo=utc)

        response = self.client.get(reverse('reservation-experiment-list', kwargs={"experiment": "e1", "recent":"/recent"}) + ".json")

        self.assertEqual(len(response.data), 1)
        experiment_names = [r['experiment']['codename'] for r in response.data]
        self.assertNotIn('e2', experiment_names)
        self.assertIn('e1', experiment_names)


class TestNowAsUtc(unittest.TestCase):

    def test_now_as_utc(self):
        now_as_utc()  # Just call see if it explodes


class TestNowField(unittest.TestCase):

    def test_returns_aware_time_row_now(self):
        now = NowField()
        self.assertTrue(is_aware(now.to_internal_value("now")))

    def test_returns_aware_time_for_datetime_str(self):
        now = NowField()
        self.assertTrue(is_aware(now.to_internal_value("2014-02-22T15:11:04.198Z")))
