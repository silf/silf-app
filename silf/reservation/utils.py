# -*- coding: utf-8 -*-
from django.utils import timezone
import datetime
import pytz


def now_as_utc():
    value = datetime.datetime.utcnow()
    value = timezone.make_aware(value, pytz.utc)
    return value