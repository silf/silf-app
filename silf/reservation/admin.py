# -*- coding: utf-8 -*-

from django.contrib import admin
from silf.reservation.models import Reservation

admin.site.register(Reservation)