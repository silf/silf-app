# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.conf.urls import url, include
from django.conf import settings
from django.utils.translation import ugettext_lazy
from django.views.generic import TemplateView

from discourse_django_sso.views import SSOClientView, SSOCreateSessionView, RedisNonceService

from rest_framework.routers import DefaultRouter

from .core.views import ExperimentViewSet
from .core.views import ServerView
from .monitoring.views import MonitoringSearch, MonitoringCreate
from silf.reservation.views import ReservationSearch, ReservationCreate
from silf.tigase.views import MyCredentialsView
from silf.experiment_results import views as result_views


default = DefaultRouter(trailing_slash=False)
default.register(r'reservation/user/(?P<user>[\w.$@+-]+)/experiment/(?P<experiment>[\w][\w\d\-_]*)(?P<recent>/recent)?', ReservationCreate, base_name="reservation-experiment-user")
# User regexp is different to allow ".json" urls.
default.register(r'reservation/user/(?P<user>[\w.$@+-]+)(?P<recent>/recent)?', ReservationSearch, base_name="reservation-user")
default.register(r'reservation/experiment/(?P<experiment>[\w][\w\d\-_]*)(?P<recent>/recent)?', ReservationSearch, base_name="reservation-experiment")
default.register("reservation(?P<recent>/recent)?", ReservationSearch, base_name="reservation")
default.register("experiments", ExperimentViewSet, base_name='experiments')
default.register("experiment", ExperimentViewSet, base_name='experiment')
default.register("monitoring", MonitoringSearch, base_name="monitoring")
default.register(r'monitoring/experiment/(?P<experiment>[\w][\w\d\-_]*)', MonitoringSearch, base_name="monitoring-experiment")
default.register(r'monitoring/event/(?P<event_type>[\w][\w\d\-_]*)', MonitoringSearch, base_name="monitoring-event")
default.register(r'monitoring/create/experiment/(?P<experiment>[\w][\w\d\-_]*)', MonitoringCreate, base_name="monitoring-create")
default.register("experiment_results/session", result_views.ExperimentSessionViewset, base_name="session")
default.register("experiment_results/series", result_views.ExperimentSeriesViewset, base_name="data-series")

api_urlpatterns = default.urls + [
    url(r'server/', ServerView.as_view()),
    url(r'server\.json', ServerView.as_view()),
    url(r'my_credentials/', MyCredentialsView.as_view(), name="my-credentials")
]

urlpatterns = [
    url("^api/", include(api_urlpatterns)),
    url(ugettext_lazy(r'^reservation/$'), TemplateView.as_view(template_name="silf/reservation/reservation.html"), name='reservation'),
    url(r'^experiment_results/$', result_views.UserDataSeriesList.as_view(), name='user-data-series'),
    url(r'^experiment_results/(?:ds/(?P<data_series>[^/.]+)/)?(?:uuid/(?P<uuid>[^/.]+)/)?$', result_views.DownloadDataSeries.as_view(), name='download-data-series'),
]

if settings.SSO_PROVIDER == 'olcms':
    nonce_service = RedisNonceService(settings.REDIS_HOST, settings.REDIS_PORT, settings.REDIS_PASSWORD)
    # nonce_service.connect()

    urlpatterns.append(url(
        r'^olcmlogin/',
        SSOClientView.as_view(
            sso_secret=settings.SSO_SECRET,
            sso_url=settings.SSO_URL,
            nonce_service=nonce_service
        ),
        name='olcmlogin'
    ))
    urlpatterns.append(url(
        r'^olcmauthorise/',
        SSOCreateSessionView.as_view(
            sso_secret=settings.SSO_SECRET,
            nonce_service=nonce_service
        ),
        name='olcmauthorise'
    ))

urlpatterns.append(url(r'^$', TemplateView.as_view(template_name='silf/core/mercury.html'), name='mercury'))
