"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.urls import reverse
from django.db.models.signals import pre_save, post_save

from django.test import TestCase
from rest_framework.test import APIClient
from silf.tigase import models
from django.db import connection
#from psycopg2 import IntegrityError
from django.db.utils import IntegrityError


class TestJIDGeneration(TestCase):

    def test_jid_generated_properly(self):

        user = get_user_model()(username="foo")
        jid = models.TigaseUser.objects.get_jid_for_django_user(user)
        self.assertEqual(jid, "user$foo@{}".format('localhost'))


class TestTigaseUserGeneration(TestCase):

    def setUp(self):
        self.user = get_user_model()(username="foo")
        self.user1 = get_user_model()(username="bar")
        post_save.disconnect(models.add_tigase_user, sender=get_user_model())

    def tearDown(self):
        super(TestTigaseUserGeneration, self).tearDown()
        post_save.connect(models.add_tigase_user, sender=get_user_model())

    def test_tigase_user_forcibly_generated(self):

        self.assertEqual(len(models.TigaseUser.objects.all()), 0)
        self.user.save()
        self.assertEqual(len(models.TigaseUser.objects.all()), 0)
        models.TigaseUser.objects.create_tigase_user_for_user(self.user)
        self.assertEqual(len(models.TigaseUser.objects.all()), 1)

    def test_tigase_user_properties(self):

        self.user.save()
        models.TigaseUser.objects.create_tigase_user_for_user(self.user)
        tig_user = models.TigaseUser.objects.all()[0]
        self.assertEqual(tig_user.django_user, self.user)
        self.user1.save()
        models.TigaseUser.objects.create_tigase_user_for_user(self.user1)
        self.assertEqual(len(models.TigaseUser.objects.all()), 2)
        tig_user, tig_user1 = models.TigaseUser.objects.all()
        self.assertNotEqual(tig_user, tig_user1)


class TestTigaseUserCreatedAutomatically(TestCase):

    def setUp(self):
        self.user = get_user_model()(username="foo")

    def test_user_created_automatically(self):
        self.assertEqual(len(models.TigaseUser.objects.all()), 0)
        self.user.save()
        self.assertEqual(len(models.TigaseUser.objects.all()), 1)


class TestMyCredentialView(TestCase):

    def setUp(self):
        self.user = User.objects.create(username="foo")
        self.client = APIClient()

    def test_my_credentials_not_authenticated(self):

        response = self.client.get(reverse('my-credentials'), )

        self.assertIn(response.status_code, (401, 403))

    def test_my_credentials_authenticated(self):
        self.client.force_authenticate(self.user)
        response = self.client.get(reverse('my-credentials'), )
        self.assertEqual(response.status_code, 200)
        data = response.data
        self.assertIsInstance(data, dict)
        self.assertEqual(data['jid'], self.user.tigase_user.user_id)
        self.assertEqual(data['password'], self.user.tigase_user.user_pw)


class TestUserCanBeDeleted(TestCase):

    def setUp(self):
        self.user = User.objects.create(username="foo")

    def test_can_delete_user(self):
        # It my explode due to tigase user
        self.user.delete()


class TestTigaseInvalidJIDUsers(TestCase):

    def setUp(self):
        pass

    def test_no_tigase_users(self):
        self.assertEqual(len(models.TigaseUser.objects.all()), 0)
        user = User.objects.create(username="foo")
        user.save()
        self.assertEqual(len(models.TigaseUser.objects.all()), 1)

    def test_valid_tigase_user(self):
        self.assertEqual(len(models.TigaseUser.objects.all()), 0)
        user = User.objects.create(username="foo")
        user.save()
        self.assertEqual(len(models.TigaseUser.objects.all()), 1)

        jid = models.TigaseUserManager.get_jid_for_django_user(user)
        self.assertEqual(jid, "user$foo@{}".format(settings.TIGASE_SERVER_DOMAIN))

        models.TigaseUserManager.create_tigase_user_simple(user, jid, "pass")

        # should not create additional TigaseUser, user with valid JID is already present
        models.add_tigase_user(user)

        self.assertEqual(len(models.TigaseUser.objects.all()), 1)

    def test_invalid_user(self):
        self.assertEqual(len(models.TigaseUser.objects.all()), 0)
        user = User.objects.create(username="foo")
        user.save()
        self.assertEqual(len(models.TigaseUser.objects.all()), 1)

        # Earlier versions of django/postgres allowed this, but since 1.9+ it fails
        # Sqlite has UNIQUE constraint on django_user_id in TigaseUser, we cannot add second user with invalid JID
        self.assertRaises(
            IntegrityError,
            models.TigaseUserManager.create_tigase_user_simple,
            user=user, jid="foo@wrongjid.zd", password="pass"
        )
