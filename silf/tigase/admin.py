# -*- coding: utf-8 -*-

from django.contrib import admin
from django.contrib.auth import get_user_model
from django.urls import reverse
from django.utils.html import escape
from django.utils.safestring import SafeString
from django.utils.translation import ugettext_lazy

from .models import TigaseUser



class TigaseUserAdmin(admin.ModelAdmin):

  readonly_fields = [
    'uid',
    'user_id',
    'acc_create_time',
    'last_login',
    'last_logout',
    'online_status',
    'failed_logins',
    'account_status',
    'django_user_link',
  ]

  fields = readonly_fields + ['user_pw']

  list_display = [
    'user_id',
  ]

  def django_user_link(self, obj):

    django_user = obj.django_user

    if django_user is None:
      return ugettext_lazy("No django user for this tig_user.")

    user_model = get_user_model()
    user_app_label = user_model._meta.app_label
    user_model_name = user_model._meta.model_name

    return SafeString(
      '<a href="{}">{}</a>'.format(
        reverse("admin:{}_{}_change".format(
          user_app_label, user_model_name), args=(django_user.pk, )
        ),
        escape(django_user.username)
      )
    )

admin.site.register(TigaseUser, TigaseUserAdmin)