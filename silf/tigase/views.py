from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework import serializers

from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.viewsets import ViewSetMixin
from silf.tigase import models


class TigaseUserSerializer(serializers.ModelSerializer):

    jid = serializers.CharField(source="user_id")
    password = serializers.CharField(source="user_pw")

    class Meta:
        fields = ("jid", "password")
        model = models.TigaseUser


class MyCredentialsView(APIView):

    permission_classes = (IsAuthenticated, )
    serializer_class = TigaseUserSerializer

    def get(self, request, *args, **kwargs):
        serializer = TigaseUserSerializer(self.get_queryset()[0])
        return Response(serializer.data)

    def get_queryset(self):
        return models.TigaseUser.objects.filter(django_user=self.request.user)

