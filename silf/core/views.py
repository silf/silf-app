from django.conf import settings

from rest_framework import viewsets
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Experiment
from .serializers import ExperimentSerializer


class ExperimentViewSet(viewsets.ReadOnlyModelViewSet):

    serializer_class = ExperimentSerializer
    queryset = Experiment.objects.all()


class ServerView(APIView):

    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        return Response({
            "domain": settings.TIGASE_SERVER_DOMAIN,
            "address": settings.TIGASE_CONNECTION_URL,
        })
