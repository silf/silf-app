# -*- coding: utf-8 -*-

from django.contrib import admin

from .models import Experiment


admin.site.register(Experiment)
