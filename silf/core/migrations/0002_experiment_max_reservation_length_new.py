# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-08-09 14:16
from __future__ import unicode_literals

import datetime
from django.db import migrations, models, connection

CREATE_CAST = r"""
CREATE FUNCTION interval_from_microseconds(data BIGINT) RETURNS INTERVAL
    AS $$SELECT data * INTERVAL '1 microsecond' $$
    LANGUAGE SQL
    IMMUTABLE
    RETURNS NULL ON NULL INPUT;

"""

DROP_CAST = r"""
DROP FUNCTION if exists interval_from_microseconds(bigint);
"""

MOVE_DATA = r"""
    UPDATE core_experiment SET max_reservation_length_new = interval_from_microseconds(max_reservation_length);
"""


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    if connection.vendor == 'postgresql':
        operations = [
            migrations.AddField(
                model_name='experiment',
                name='max_reservation_length_new',
                field=models.DurationField(default=datetime.timedelta(0, 7200), verbose_name='Maximal time for experiment'),
            ),
            migrations.RunSQL(CREATE_CAST),
            migrations.RunSQL(MOVE_DATA),
            migrations.RemoveField(
                model_name='experiment',
                name='max_reservation_length',
            ),
            migrations.RenameField(
                model_name='experiment',
                old_name='max_reservation_length_new',
                new_name='max_reservation_length',
            ),
            migrations.RunSQL(DROP_CAST),
        ]
    else:
        operations = [
            migrations.AddField(
                model_name='experiment',
                name='max_reservation_length_new',
                field=models.DurationField(default=datetime.timedelta(0, 7200),
                                           verbose_name='Maximal time for experiment'),
            ),
            migrations.RemoveField(
                model_name='experiment',
                name='max_reservation_length',
            ),
            migrations.RenameField(
                model_name='experiment',
                old_name='max_reservation_length_new',
                new_name='max_reservation_length',
            ),
        ]
