# -*- coding: utf-8 -*-

import datetime

from django.conf import settings

from django.db import models

from django.utils.translation import ugettext_lazy

from django.db.models.fields import DurationField, BigIntegerField


class Experiment(models.Model):

    id = models.AutoField(primary_key=True)
    codename = models.SlugField(max_length=200, unique=True, verbose_name=ugettext_lazy(u"Experiment codename"))
    display_name = models.CharField(
        max_length=500, unique=True, verbose_name=ugettext_lazy(u"Human-readable experiment name"),
        null=False, blank=False)

    max_reservation_length = DurationField(
        verbose_name=ugettext_lazy(u"Maximal time for experiment"), default=datetime.timedelta(hours=2))
    camera_url = models.CharField(
        verbose_name=ugettext_lazy(u'Camera address'), max_length=50, blank=True, null=True)
    docs_url = models.CharField(
        verbose_name=ugettext_lazy(u'Instruction address'), max_length=50, blank=True, null=True)

    @property
    def url(self):
        return '%s@muc.%s' % (self.codename, settings.TIGASE_SERVER_DOMAIN)

    def __str__(self):
        return self.codename
