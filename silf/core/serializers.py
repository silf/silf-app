from rest_framework import serializers

from .models import Experiment


from rest_framework.fields import Field, IntegerField
from datetime import timedelta

from django.utils.translation import get_language

from page_block.models import PageBlock


class TimedeltaSerializer(Field):
    """
    >>> ser = TimedeltaSerializer()
    >>> ser.to_representation(timedelta(hours=1))
    3600
    >>> ser.to_internal_value(3600) == timedelta(hours=1)
    True
    """

    def to_internal_value(self, value):
        return timedelta(minutes=value)

    def to_representation(self, data):
        return data.seconds/60


class ExperimentSerializer(serializers.ModelSerializer):

    """
    >>> experiment = Experiment(codename="exp", display_name="Experiment", max_reservation_length=timedelta(seconds=100))
    >>> ExperimentSerializer(experiment).data == {
    ...    'codename': u'exp', 'display_name': u'Experiment',
    ...     'max_reservation_length': 100
    ... }
    True
    """

    url = serializers.ReadOnlyField()
    max_reservation_length = TimedeltaSerializer()

    def to_representation(self, instance):
        ret = super().to_representation(instance)

        language = get_language()
        pb = PageBlock.objects.get_language_version(ret['docs_url'], language)

        ret['docs_url'] = str(pb.url) if pb else None
        return ret

    class Meta:
        model = Experiment
        fields = '__all__'

